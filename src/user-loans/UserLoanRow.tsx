import React, { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { GroupedLoan, Loan } from '../api/dto/Loan';
import { Button } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';

const UserLoanRow = ({
  loan,
  handle,
  label,
  predicate,
}: {
  loan: Loan;
  handle: (loan: Loan) => void;
  label: string;
  predicate: (loan: Loan) => boolean;
}) => {
  const navigate = useNavigate();
  const [t, i18n] = useTranslation();
  const api = useApi();

  return (
    <>
      <tr className="book-row">
        <th scope="row">{loan.user?.username}</th>
        <td>{loan.borrowedBook?.title}</td>
        <td>{loan.loanDate}</td>
        <td>{loan.returnDeadline}</td>
        <td>{loan.returnDate}</td>
        <td>
          <Button
            variant="contained"
            onClick={() => handle(loan)}
            disabled={predicate(loan)}
          >
            {label}
          </Button>
        </td>
      </tr>
    </>
  );
};

export default UserLoanRow;
