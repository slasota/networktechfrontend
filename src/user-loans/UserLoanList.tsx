import { GroupedLoan, Loan } from '../api/dto/Loan';
import './UserLoanList.css';
import { useTranslation } from 'react-i18next';
import UserLoanRow from './UserLoanRow';

const UserLoanList = ({
  loans,
  handle,
  label,
  predicate = () => true,
}: {
  loans: Loan[];
  handle: (loan: Loan) => void;
  label: string;
  predicate: (loan: Loan) => boolean;
}) => {
  const [t, i18n] = useTranslation();
  return (
    <>
      <table className="fl-table">
        <thead>
          <tr>
            <th scope="col">{t('Username')}</th>
            <th scope="col">{t('Title')}</th>
            <th scope="col">{t('LoanDate')}</th>
            <th scope="col">{t('ReturnDeadline')}</th>
            <th scope={'col'}>{t('ReturnDate')}</th>
            <th scope={'col'}>{label}</th>
          </tr>
        </thead>
        <tbody>
          {loans.map((it) => (
            <UserLoanRow
              key={it.user?.username}
              loan={it}
              handle={handle}
              label={label}
              predicate={predicate}
            />
          ))}
        </tbody>
      </table>
    </>
  );
};

export default UserLoanList;
