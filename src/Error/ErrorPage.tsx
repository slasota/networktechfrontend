import { Button } from '@mui/material';
import { useCallback } from 'react';
import { useApi } from '../api/ApiProvider';
import { useNavigate } from 'react-router-dom';
import './ErrorPage.css';
import { useTranslation } from 'react-i18next';

const ErrorPage = ({ message }: { message: string }) => {
  const api = useApi();
  const navigate = useNavigate();
  const [t, i18n] = useTranslation();

  const onLoginClick = useCallback(() => {
    api.logout();
    navigate('/');
  }, [api, navigate]);

  return (
    <>
      <div className={'error-msg'}>
        <h1>
          {t('Error')}: {message}.
        </h1>
        {t('tryAgainLater')}
        <Button variant="contained" onClick={onLoginClick}>
          {t('Login')}
        </Button>
      </div>
    </>
  );
};

export default ErrorPage;
