import React, { useCallback } from 'react';
import './WelcomePage.css';
import BookList from '../book-view/BookList';
import LoanList from '../loan-view/LoanList';
import AccountInfo from './AccountInfo';
import { useLocation, useNavigate } from 'react-router-dom';
import { User } from '../api/dto/User';
import { useApi } from '../api/ApiProvider';
import { useTranslation } from 'react-i18next';
import UserLoanList from '../user-loans/UserLoanList';
import { Loan } from '../api/dto/Loan';

const WelcomePage = () => {
  const api = useApi();
  const location = useLocation();
  const navigate = useNavigate();
  const [t, i18n] = useTranslation();

  const user = location.state ? location.state.user : null;
  const loans = location.state
    ? location.state.loans
      ? location.state.loans
      : []
    : [];

  const handle = useCallback(
    (loan: Loan) => {
      const date = new Date();
      loan.returnDate = `${date.getFullYear()}-${(
        date.getMonth() + 1
      ).toLocaleString('en-US', {
        minimumIntegerDigits: 2,
        useGrouping: false,
      })}-${date.getDate().toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })}`;
      api.patchLoan(loan).then((response) => {
        if (!response.success) {
          return;
        }
        api
          .getSelfInfo()
          .then((response) => {
            navigate('/Home', {
              state: {
                loans: response.data?.loans,
                user: response.data?.user,
              },
            });
          })
          .catch((err) => console.log(err));
      });
      console.log(loan);
    },
    [api, navigate],
  );

  return (
    <>
      <h1 className="header">{t('Welcome')}</h1>
      <div className="dash-board">
        <div className="dash-board-column">
          <h2 className="header">{t('Account')}</h2>
          <AccountInfo user={user} />
        </div>
        <div className="dash-board-column">
          <h2 className="header">{t('YourBooks')}</h2>
          <UserLoanList
            loans={loans}
            label={t('Return')}
            handle={handle}
            predicate={(loan: Loan) => loan.returnDate !== null}
          />
        </div>
      </div>
    </>
  );
};

export default WelcomePage;
