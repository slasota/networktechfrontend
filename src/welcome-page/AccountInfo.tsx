import React, { useCallback } from 'react';
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@mui/material';
import { useApi } from '../api/ApiProvider';
import { useNavigate } from 'react-router-dom';
import { User } from '../api/dto/User';
import { useTranslation } from 'react-i18next';

const AccountInfo = ({ user }: { user: User }) => {
  const api = useApi();
  const navigate = useNavigate();

  const [t, i18n] = useTranslation();

  const onLogOut = useCallback(() => {
    api.logout();
    navigate('/login');
  }, [api, navigate]);

  return (
    <div className="account-info">
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>
              {user ? t('Hello') + user.username : t('NotLogged')}
            </TableCell>
            <TableCell align="right">
              <Button variant="contained" onClick={onLogOut}>
                {user ? t('Logout') : t('Login')}
              </Button>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell>{t('eMail')}</TableCell>
            <TableCell align="right">{user ? user.eMail : 'N/A'}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>{t('Role')}</TableCell>
            <TableCell align="right">{user ? user.role : 'N/A'}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell>{t('FullUsername')}</TableCell>
            <TableCell align="right">
              {user ? user.fullUsername : 'N/A'}
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </div>
  );
};

export default AccountInfo;
