import React from 'react';
import './App.css';
import LoginForm from './login-form/LoginForm';
import { Navigate, Route, Router, Routes } from 'react-router-dom';
import WelcomePage from './welcome-page/WelcomePage';
import NavBar from './nav/NavBar';
import DetailedBookView from './book-view/DetailedBookView';
import LoanList from './loan-view/LoanList';
import LoanDetailedView from './loan-view/LoanDetailedView';
import BookPage from './book-view/BookPage';
import ApiProvider from './api/ApiProvider';
import UserPage from './loan-view/UserPage';
import AddBook from './book-view/AddBook';
import AddUser from './loan-view/AddUser';
import I18n from './i18n';
import { I18nextProvider } from 'react-i18next';
import EditBook from './book-view/EditBook';

function App() {
  return (
    <>
      <I18nextProvider i18n={I18n}>
        <ApiProvider>
          <NavBar />
          <Routes>
            <Route path="" element={<Navigate to="/login" />} />
            <Route path="Books" element={<BookPage />} />
            <Route path="login" element={<LoginForm />} />
            <Route path="newBook" element={<AddBook />} />
            <Route path="newuser" element={<AddUser />} />
            <Route path="Home" element={<WelcomePage />} />
            <Route path="DetailedView" element={<DetailedBookView />} />
            <Route path="Loans" element={<UserPage />} />
            <Route path="DetailedLoan" element={<LoanDetailedView />} />
            <Route path="EditBook" element={<EditBook />} />
          </Routes>
        </ApiProvider>
      </I18nextProvider>
    </>
  );
}

export default App;
