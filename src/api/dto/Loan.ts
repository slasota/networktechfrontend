import { Book } from './Book';
import { User } from './User';

export class Loan {
  id: number | undefined;
  borrowedBook: Book | undefined;
  user: User | undefined;
  loanDate: string | undefined;
  returnDeadline: string | undefined;
  returnDate: string | undefined;
}

export class GroupedLoan {
  user: User | undefined;
  loans: Loan[] | undefined;
}
