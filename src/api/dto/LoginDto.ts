import { User } from './User';
import { Book } from './Book';
import { Loan } from './Loan';

export class LoginDto {
  username: string | undefined;
  password: string | undefined;
}

export class LoginResponseDto {
  token: string | undefined;
  user: User | undefined;
}

export class StartupResponse {
  user: User | undefined;
  loans: Loan[] | undefined;
}
