export class Book {
  id: number | undefined;
  title: string | undefined;
  author: string | undefined;
  isbn: string | undefined;
  publisher: string | undefined;
  yearOfPublish: number | undefined;
  availableCopies: number | undefined;
}
