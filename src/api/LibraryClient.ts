import axios, { Axios, AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import { LoginDto, LoginResponseDto, StartupResponse } from './dto/LoginDto';
import { User } from './dto/User';
import { Book } from './dto/Book';
import { GroupedLoan, Loan } from './dto/Loan';

export type ClientResponse<T> = {
  success: boolean;
  data: T;
  statusCode: number;
};

export class LibraryClient {
  private client: AxiosInstance;

  constructor() {
    this.client = axios.create({
      baseURL: 'http://localhost:8080',
    });
  }

  public async getSelfInfo(): Promise<ClientResponse<StartupResponse | null>> {
    try {
      const response: AxiosResponse<User> = await this.client.get('/user/get');
      console.log('response', response);
      const loans: AxiosResponse<Loan[]> = await this.client.get('/loan/forMe');

      return {
        success: true,
        data: {
          user: response.data,
          loans: loans.data,
        },
        statusCode: response.status,
      };
    } catch (err) {
      console.log('err', err);
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  public async login(
    data: LoginDto,
  ): Promise<ClientResponse<StartupResponse | null>> {
    try {
      const response: AxiosResponse<LoginResponseDto> = await this.client.post(
        '/login',
        data,
      );
      this.client.defaults.headers.common['Authorization'] =
        `Bearer ${response.data.token}`;

      const loans: AxiosResponse<Loan[]> = await this.client.get('/loan/forMe');

      return {
        success: true,
        data: {
          user: response.data.user,
          loans: loans.data,
        },
        statusCode: response.status,
      };
    } catch (err) {
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  public async addBook(book: Book): Promise<ClientResponse<Book | null>> {
    try {
      const response: AxiosResponse<Book> = await this.client.post(
        '/book/add',
        book,
      );

      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (err) {
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  public async logout() {
    delete this.client.defaults.headers.common['Authorization'];
  }

  public async getBooksForUser() {
    try {
      const response: AxiosResponse<Book[]> =
        await this.client.post('/books/getByUser');
      console.log(response);
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (err) {
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  public async getBooks(): Promise<ClientResponse<Book[] | null>> {
    try {
      const booksResponse: AxiosResponse<Book[]> =
        await this.client.get('book/all');
      return {
        success: true,
        data: booksResponse.data,
        statusCode: booksResponse.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  public async getLoans(): Promise<ClientResponse<GroupedLoan[] | null>> {
    try {
      const loans: AxiosResponse<GroupedLoan[]> =
        await this.client.get('/user/usersLoans');
      return {
        success: true,
        data: loans.data,
        statusCode: loans.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  public async addUser(user: User): Promise<ClientResponse<User | null>> {
    try {
      const response: AxiosResponse<User> = await this.client.post(
        '/user/add',
        user,
      );

      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (err) {
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  public async borrow(id: number): Promise<ClientResponse<Loan | null>> {
    try {
      const date = new Date();
      const returnDate = new Date(date.getTime() + 1000 * 60 * 60 * 24 * 14);
      let data = {
        user: {},
        borrowedBook: { id: id },
        loanDate: `${date.getFullYear()}-${(date.getMonth() + 1).toLocaleString(
          'en-US',
          {
            minimumIntegerDigits: 2,
            useGrouping: false,
          },
        )}-${date.getDate().toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })}`,
        returnDeadline: `${returnDate.getFullYear()}-${(
          returnDate.getMonth() + 1
        ).toLocaleString('en-US', {
          minimumIntegerDigits: 2,
          useGrouping: false,
        })}-${returnDate.getDate().toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })}`,
      };
      console.log(data);
      const response: AxiosResponse<Loan> = await this.client.post(
        '/loan/add',
        data,
      );

      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (err) {
      console.log(err);
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  async deleteUser(
    username: string | undefined,
  ): Promise<ClientResponse<string | null>> {
    try {
      const response: AxiosResponse<string> = await this.client.post(
        '/user/delete',
        { username: username },
      );

      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (err) {
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  async deleteBook(book: Book | null): Promise<ClientResponse<Book | null>> {
    try {
      const response = await this.client.post('/book/delete', book);
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (err) {
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  async getBook(id: number) {
    try {
      const response = await this.client.get('/book/getForEdit', {
        params: { id: id },
      });
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (err) {
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  async patchLoan(loan: Loan) {
    try {
      const response: AxiosResponse<Loan> = await this.client.post(
        '/loan/patch',
        loan,
      );

      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (err) {
      console.log(err);
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  async getLoansForUser(username: string) {
    try {
      const response = await this.client.get('/loan/byUsername', {
        params: { username: username },
      });
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (err) {
      const axiosError = err as AxiosError<Error>;

      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }
}
