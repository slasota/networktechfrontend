import LoanRow from './LoanRow';
import { GroupedLoan } from '../api/dto/Loan';
import { useTranslation } from 'react-i18next';

const LoanList = ({ loans }: { loans: GroupedLoan[] }) => {
  const [t, i18n] = useTranslation();
  return (
    <>
      <table className="fl-table">
        <thead>
          <tr>
            <th scope="col">{t('Username')}</th>
            <th scope="col">{t('eMail')}</th>
            <th scope="col">{t('Role')}</th>
            <th scope="col">{t('NumBorrowed')}</th>
            <th scope={'col'}>{t('Delete')}</th>
          </tr>
        </thead>
        <tbody>
          {loans.map((it) => (
            <LoanRow key={it.user?.username} group={it} />
          ))}
        </tbody>
      </table>
    </>
  );
};

export default LoanList;
