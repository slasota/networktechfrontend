import React, { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { GroupedLoan } from '../api/dto/Loan';
import { Button } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';

const LoanRow = ({ group }: { group: GroupedLoan }) => {
  const navigate = useNavigate();
  const [t, i18n] = useTranslation();
  const api = useApi();

  const onClick = useCallback(() => {
    console.log(group.user?.username);
    api.getLoansForUser(group.user?.username!).then((response) => {
      if (!response.success) {
        return;
      }
      navigate('/DetailedLoan', {
        state: { username: group.user?.username, loans: response.data },
      });
    });
  }, [api, group.user?.username, navigate]);

  const onDelete = useCallback(() => {
    api.deleteUser(group.user?.username).then((response) => {
      if (!response.success) {
        console.log('err');
        return;
      }
      api
        .getLoans()
        .then((response) => {
          navigate('/Loans', {
            state: { loans: response.data, success: response.success },
          });
        })
        .catch((err) => console.log(err));
    });
  }, [api, group.user?.username, navigate]);

  return (
    <>
      <tr className="book-row">
        <th onClick={onClick} scope="row">
          {group.user?.username}
        </th>
        <td onClick={onClick}>{group.user?.eMail}</td>
        <td onClick={onClick}>
          {group.user?.role?.split('_')[1].toLocaleLowerCase()}
        </td>
        <td onClick={onClick}>{group.loans?.length}</td>
        <td>
          <Button variant="contained" onClick={onDelete}>
            {t('Delete')}
          </Button>
        </td>
      </tr>
    </>
  );
};

export default LoanRow;
