import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate } from 'react-router-dom';
import { Loan } from '../api/dto/Loan';
import UserLoanList from '../user-loans/UserLoanList';
import { useCallback } from 'react';
import { bool } from 'yup';
import { useApi } from '../api/ApiProvider';

const LoanDetailedView = () => {
  const [t, i18n] = useTranslation();
  const location = useLocation();
  const api = useApi();
  const navigate = useNavigate();

  const username: string = location.state.username;
  const loans: Loan[] = location.state.loans;

  const handle = useCallback(
    (loan: Loan) => {
      loan.returnDate = undefined;
      console.log(username);
      api.patchLoan(loan).then((response) => {
        if (!response.success) {
          return;
        }
        api.getLoansForUser(username).then((response) => {
          if (!response.success) {
            return;
          }
          console.log(response);
          navigate('/DetailedLoan', {
            state: { username: username, loans: response.data },
          });
        });
      });
    },
    [api, navigate, username],
  );

  const predicate = (loan: Loan): boolean => {
    return (
      loan.returnDate === undefined ||
      loan.returnDate === null ||
      new Date(loan.returnDate).getTime() + 3 * 24 * 60 * 60 * 1000 <
        new Date().getTime()
    );
  };

  return (
    <>
      <div className={'details-headers'}>
        <h1>{`${t('detailedHeader')}${username}`}</h1>
      </div>
      <UserLoanList
        loans={loans}
        handle={handle}
        label={t('Cancel')}
        predicate={predicate}
      />
    </>
  );
};

export default LoanDetailedView;
