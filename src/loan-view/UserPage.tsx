import { useLocation, useNavigate } from 'react-router-dom';
import { Button, TextField } from '@mui/material';
import LoanList from './LoanList';
import './UserPage.css';
import ErrorPage from '../Error/ErrorPage';
import { useTranslation } from 'react-i18next';
import { useState } from 'react';
import { GroupedLoan } from '../api/dto/Loan';

const UserPage = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [t, i18n] = useTranslation();

  const [crit, setCrit] = useState('');

  const loans: GroupedLoan[] = location.state ? location.state.loans : [];
  const success = location.state ? location.state.success : false;

  if (success) {
    return (
      <>
        <div className="users-header">
          <Button variant="contained" onClick={() => navigate('/newUser')}>
            {t('AddUser')}
          </Button>
          <TextField
            id="filter"
            label="Search"
            variant="standard"
            onChange={(e) => setCrit(e.target.value)}
          />
        </div>
        <LoanList
          loans={loans.filter((it) =>
            it.user?.username?.toLowerCase().includes(crit.toLowerCase()),
          )}
        />
      </>
    );
  } else {
    return (
      <>
        <ErrorPage message={t('Unauthorized')} />
      </>
    );
  }
};

export default UserPage;
