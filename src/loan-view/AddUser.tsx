import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  Input,
  InputLabel,
  Select,
  SelectChangeEvent,
  styled,
  TextField,
} from '@mui/material';
import React, { useCallback, useState } from 'react';
import { Book } from '../api/dto/Book';
import './AddUser.css';
import { useApi } from '../api/ApiProvider';
import CloseIcon from '@mui/icons-material/Close';
import { useNavigate } from 'react-router-dom';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { User } from '../api/dto/User';
import MenuItem from '@mui/material/MenuItem';
import { useTranslation } from 'react-i18next';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

const AddUser = () => {
  const api = useApi();
  const navigate = useNavigate();
  const [t, i18n] = useTranslation();

  const [open, setOpen] = React.useState(false);
  const [message, setMsg] = useState('');

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const [user, setBook] = useState(new User());
  const [role, setRole] = useState('');

  const submitBook = useCallback(() => {
    api.addUser(user).then((result) => {
      if (result.success) {
        setMsg(`${t('UserCreationSuccess')} ${result.data?.username}`);
      } else {
        setMsg(t('UserCreationFailure'));
      }
      handleClickOpen();
    });
    console.log(user);
  }, [api, t, user]);

  return (
    <>
      <div className="new-user-header">
        <h1>{t('NewUserHeader')}</h1>
        {t('NewUserReminder')}
      </div>
      <div className="new-user-container">
        <div className="new-user-row">
          <TextField
            id="username"
            label={t('Username')}
            variant="standard"
            onChange={(e) => (user.username = e.target.value)}
          />
          <TextField
            id="eMail"
            label={t('eMail')}
            variant="standard"
            onChange={(e) => (user.eMail = e.target.value)}
          />
        </div>
        <div className="new-user-row">
          <TextField
            id="password"
            label={t('Password')}
            variant="standard"
            onChange={(e) => (user.password = e.target.value)}
          />
          <TextField
            id="fullUsername"
            label={t('FullUsername')}
            variant="standard"
            onChange={(e) => (user.fullUsername = e.target.value)}
          />
        </div>
        <div className="new-user-row">
          <FormControl className="new-user-combo">
            <InputLabel id="demo-simple-select-label">Role</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={role}
              label={t('Role')}
              onChange={(e: SelectChangeEvent<string>) => {
                setRole(e.target.value);
                user.role = e.target.value;
                console.log(user);
              }}
            >
              <MenuItem value={'ROLE_WORKER'}>{t('Worker')}</MenuItem>
              <MenuItem value={'ROLE_USER'}>{t('User')}</MenuItem>
            </Select>
          </FormControl>
        </div>
        <Button
          variant={'contained'}
          className="new-user-submit"
          onClick={submitBook}
        >
          {t('Add')}
        </Button>
      </div>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
          {t('Result')}
        </DialogTitle>
        <IconButton
          aria-label="close"
          onClick={handleClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
        <DialogContent dividers>{message}</DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose}>
            Ok
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </>
  );
};

export default AddUser;
