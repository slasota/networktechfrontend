import React, { useCallback, useMemo } from 'react';
import './LoginForm.css';
import { Button, TextField } from '@mui/material';
import { Formik } from 'formik';
import * as yup from 'yup';
import { useNavigate } from 'react-router-dom';
import { useApi } from '../api/ApiProvider';
import { useTranslation } from 'react-i18next';

const LoginForm = () => {
  const navigate = useNavigate();
  const [t, i18n] = useTranslation();
  const api = useApi();

  const onSubmit = useCallback(
    (values: { username: string; password: string }, formik: any) => {
      api
        .login(values)
        .then((response) => {
          if (response.success) {
            navigate('/home', {
              state: {
                user: response.data?.user,
                loans: response.data?.loans,
              },
            });
          } else {
            formik.setFieldError('username', 'Invalid username');
          }
        })
        .catch((err) => console.log('bla bla', err));
    },
    [api, navigate],
  );

  const validationSchema = useMemo(
    () =>
      yup.object().shape({
        username: yup.string().required('Username is required'),
        password: yup
          .string()
          .required('Password is required')
          .min(8, t('PasswordContains')),
      }),
    [],
  );

  return (
    <div>
      <Formik
        initialValues={{ username: '', password: '' }}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
        validateOnChange
        validateOnBlur
      >
        {(formik: any) => (
          <form
            className="login-form"
            id="signForm"
            noValidate
            onSubmit={formik.handleSubmit}
          >
            <TextField
              id="username"
              label={t('Username')}
              variant="outlined"
              name="username"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.username && !!formik.errors.username}
              helperText={formik.touched.username && formik.errors.username}
            />
            <TextField
              id="password"
              label={t('Password')}
              variant="outlined"
              type="password"
              name="password"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.password && !!formik.errors.password}
              helperText={formik.touched.password && formik.errors.password}
            />
            <Button
              variant="contained"
              type="submit"
              form="signForm"
              disabled={!(formik.isValid && formik.dirty)}
            >
              {t('Login')}
            </Button>
          </form>
        )}
      </Formik>
    </div>
  );
};
export default LoginForm;
