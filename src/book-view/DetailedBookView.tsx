import { useLocation, useNavigate } from 'react-router-dom';
import { Button } from '@mui/material';
import { useCallback, useState } from 'react';
import { useApi } from '../api/ApiProvider';
import { useTranslation } from 'react-i18next';
import './DetailedBookView.css';

const DetailedBookView = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const book = location.state ? location.state.book : null;
  const api = useApi();
  const [t, i18n] = useTranslation();

  const [status, setStatus] = useState('');
  const [col, setCol] = useState('green');

  const handleBorrow = useCallback(() => {
    api.borrow(book.id).then((response) => {
      console.log(response);
      if (!response.success && response.statusCode === 400) {
        setStatus(t('borrowedBooks'));
        setCol('red');
        return;
      }
      if (!response.success) {
        setStatus(t('notLogged'));
        setCol('red');
        return;
      }
      setCol('green');
      setStatus(`${t('borrowedSuccess')}${response.data?.returnDeadline}`);
    });
  }, [api, book.id, t]);

  const handleDelete = useCallback(() => {
    api.deleteBook(book).then((response) => {
      if (!response.success) {
        setStatus(t('UnauthorizedDelete'));
        setCol('red');
        return;
      }

      api
        .getBooks()
        .then((response) => {
          navigate('/Books', { state: { books: response.data } });
        })
        .catch((err) => console.log(err));
    });
  }, [api, book, navigate, t]);

  const handleChange = useCallback(() => {
    api.getBook(book.id).then((response) => {
      if (!response.success) {
        setStatus(t('UnauthorizedEdit'));
        setCol('red');
        return;
      }
      navigate('/EditBook', { state: { book: book } });
    });
  }, [api, book, navigate, t]);

  return (
    <div className="view-container">
      <div className={'container-row'} style={{ color: col }}>
        {status}
      </div>
      <div className="container-row">
        <p className="row-title">
          {t('Title')}: {book?.title}
        </p>
      </div>
      <div className="container-row">
        <p className="row-text">
          {t('Author')}: {book?.author}
        </p>
      </div>
      <div className="container-row">
        <p className="row-text">ISBN: {book?.isbn}</p>
      </div>
      <div className="container-row">
        <p className="row-text">
          {t('Publisher')}: {book?.publisher}
        </p>
      </div>
      <div className="container-row">
        <p className="row-text">
          {t('YearOfPublish')}: {book?.yearOfPublish}
        </p>
      </div>
      <div className="container-row">
        <p className="row-text">
          {t('NumberOfCopies')}: {book?.availableCopies}
        </p>
      </div>
      <div className="container-row">
        <Button onClick={handleBorrow} size={'large'} variant={'contained'}>
          {t('Borrow')}
        </Button>
        <Button onClick={handleChange} size={'large'} variant={'contained'}>
          {t('Modify')}
        </Button>
        <Button onClick={handleDelete} size={'large'} variant={'contained'}>
          {t('Delete')}
        </Button>
      </div>
    </div>
  );
};

export default DetailedBookView;
