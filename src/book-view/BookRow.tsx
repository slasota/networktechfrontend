import React, { useCallback } from 'react';
import { Button } from '@mui/material';
import './BookRow.css';
import { useNavigate } from 'react-router-dom';
import { Book } from '../api/dto/Book';

const BookRow = (book: Book) => {
  const navigate = useNavigate();
  const onClick = useCallback(() => {
    console.log(book.isbn);
    navigate('/DetailedView', {
      state: {
        book: book,
      },
    });
  }, []);

  return (
    <>
      <tr onClick={onClick} className="book-row">
        <th scope="row">{book.isbn}</th>
        <td>{book.author}</td>
        <td>{book.title}</td>
        <td>{book.publisher}</td>
        <td>{book.availableCopies}</td>
      </tr>
    </>
  );
};

export default BookRow;
