import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Input,
  styled,
  TextField,
} from '@mui/material';
import React, { useCallback, useState, useTransition } from 'react';
import { Book } from '../api/dto/Book';
import './AddBook.css';
import { useApi } from '../api/ApiProvider';
import CloseIcon from '@mui/icons-material/Close';
import { useNavigate } from 'react-router-dom';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

const AddBook = () => {
  const api = useApi();
  const navigate = useNavigate();
  const [t, i18n] = useTranslation();

  const [open, setOpen] = React.useState(false);
  const [message, setMsg] = useState('');

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const [book, setBook] = useState(new Book());

  const submitBook = useCallback(() => {
    api.addBook(book).then((result) => {
      if (result.success) {
        setMsg(`${t('BookCreationSuccess')}: ${result.data}`);
      } else {
        setMsg(`${t('BookCreationFailure')}`);
      }
      handleClickOpen();
    });
    console.log(book);
  }, [t, api, book]);

  return (
    <>
      <div className="new-book-header">
        <h1>{t('AddNewBookHeader')}</h1>
        {t('AddNewBookReminder')}
      </div>
      <div className="new-book-container">
        <div className="new-book-row">
          <TextField
            id="isbn"
            label="ISBN"
            variant="standard"
            onChange={(e) => (book.isbn = e.target.value)}
          />
          <TextField
            id="title"
            label={t('Title')}
            variant="standard"
            onChange={(e) => (book.title = e.target.value)}
          />
        </div>
        <div className="new-book-row">
          <TextField
            id="author"
            label={t('Author')}
            variant="standard"
            onChange={(e) => (book.author = e.target.value)}
          />
          <TextField
            id="publisher"
            label={t('Publisher')}
            variant="standard"
            onChange={(e) => (book.publisher = e.target.value)}
          />
        </div>
        <div className="new-book-row">
          <TextField
            id="yearOfPublish"
            label={t('YearOfPublish')}
            variant="standard"
            type={'number'}
            onChange={(e) => (book.yearOfPublish = +e.target.value)}
          />
          <TextField
            id="numberOfCopies"
            label={t('NumberOfCopies')}
            variant="standard"
            type={'number'}
            onChange={(e) => (book.availableCopies = +e.target.value)}
          />
        </div>
        <Button
          variant={'contained'}
          className="new-book-submit"
          onClick={submitBook}
        >
          {t('Add')}
        </Button>
      </div>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
          {t('Result')}
        </DialogTitle>
        <IconButton
          aria-label="close"
          onClick={handleClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
        <DialogContent dividers>{message}</DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose}>
            Ok
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </>
  );
};

export default AddBook;
