import { Book } from '../api/dto/Book';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  styled,
  TextField,
} from '@mui/material';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate } from 'react-router-dom';
import { useApi } from '../api/ApiProvider';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

const EditBook = () => {
  const [t, i18n] = useTranslation();
  const location = useLocation();
  const navigate = useNavigate();
  const api = useApi();

  const [book, setBook] = useState(location.state.book);

  const id = book.id;
  const [isbn, setisbn] = useState(book.isbn);
  const [title, settitle] = useState(book.title);
  const [author, setauthor] = useState(book.author);
  const [publisher, setpublisher] = useState(book.publisher);
  const [yearOfPublish, setyearOfPublish] = useState(book.yearOfPublish);
  const [availableCopies, setavailableCopies] = useState(book.availableCopies);

  const [open, setOpen] = React.useState(false);
  const [message, setMsg] = useState('');

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const submitBook = useCallback(() => {
    const book = new Book();
    book.id = id;
    book.isbn = isbn;
    book.title = title;
    book.author = author;
    book.publisher = publisher;
    book.yearOfPublish = yearOfPublish;
    book.availableCopies = availableCopies;
    api.addBook(book).then((response) => {
      if (!response.success) {
        setMsg(t('UnauthorizedEdit'));
        setOpen(true);
        return;
      }
      api
        .getBooks()
        .then((response) => {
          navigate('/Books', { state: { books: response.data } });
        })
        .catch((err) => console.log(err));
    });
  }, [
    api,
    author,
    availableCopies,
    id,
    isbn,
    navigate,
    publisher,
    t,
    title,
    yearOfPublish,
  ]);

  return (
    <>
      <div className="new-book-header">
        <h1>{t('AddNewBookHeader')}</h1>
        {t('AddNewBookReminder')}
      </div>
      <div className="new-book-container">
        <div className="new-book-row">
          <TextField
            id="isbn"
            label="ISBN"
            variant="standard"
            value={isbn}
            onChange={(e) => setisbn(e.target.value)}
          />
          <TextField
            id="title"
            label={t('Title')}
            variant="standard"
            value={title}
            onChange={(e) => settitle(e.target.value)}
          />
        </div>
        <div className="new-book-row">
          <TextField
            id="author"
            label={t('Author')}
            variant="standard"
            value={author}
            onChange={(e) => setauthor(e.target.value)}
          />
          <TextField
            id="publisher"
            label={t('Publisher')}
            variant="standard"
            value={publisher}
            onChange={(e) => setpublisher(e.target.value)}
          />
        </div>
        <div className="new-book-row">
          <TextField
            id="yearOfPublish"
            label={t('YearOfPublish')}
            variant="standard"
            type={'number'}
            value={yearOfPublish}
            onChange={(e) => setyearOfPublish(+e.target.value)}
          />
          <TextField
            id="numberOfCopies"
            label={t('NumberOfCopies')}
            variant="standard"
            type={'number'}
            value={availableCopies}
            onChange={(e) => setavailableCopies(+e.target.value)}
          />
        </div>
        <Button
          variant={'contained'}
          className="new-book-submit"
          onClick={submitBook}
        >
          {t('Confirm')}
        </Button>
      </div>

      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
          {t('Result')}
        </DialogTitle>
        <IconButton
          aria-label="close"
          onClick={handleClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
        <DialogContent dividers>{message}</DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose}>
            Ok
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </>
  );
};

export default EditBook;
