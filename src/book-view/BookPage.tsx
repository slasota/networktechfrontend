import { Button, TextField } from '@mui/material';
import BookList from './BookList';
import './BookPage.css';

import { useLocation, useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useState } from 'react';
import { Book } from '../api/dto/Book';

const BookPage = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [t, i18n] = useTranslation();

  const [crit, setCrit] = useState('');

  const books: Book[] = location.state ? location.state.books : [];
  return (
    <>
      <div className="book-header">
        <Button variant="contained" onClick={() => navigate('/newBook')}>
          {t('AddBook')}
        </Button>
        <TextField
          id="filter"
          label={t('Search')}
          variant="standard"
          onChange={(e) => setCrit(e.target.value)}
        />
      </div>
      <BookList
        bookList={books.filter(
          (it) =>
            it.title?.toLowerCase().includes(crit.toLowerCase()) ||
            it.author?.toLowerCase().includes(crit.toLowerCase()) ||
            it.isbn?.toLowerCase().includes(crit.toLowerCase()),
        )}
      />
    </>
  );
};

export default BookPage;
