import React from 'react';
import './BookList.css';
import BookRow from './BookRow';
import { string } from 'yup';
import { Book } from '../api/dto/Book';
import { useTranslation } from 'react-i18next';

const BookList = ({ bookList }: { bookList: Array<Book> }) => {
  const [t, i18n] = useTranslation();
  return (
    <>
      <table className="fl-table">
        <thead>
          <tr>
            <th scope="col">ISBN</th>
            <th scope="col">{t('Author')}</th>
            <th scope="col">{t('Title')}</th>
            <th scope="col">{t('Publisher')}</th>
            <th scope="col">{t('NumberOfCopies')}</th>
          </tr>
        </thead>
        <tbody>
          {bookList.map((book) => (
            <BookRow key={book.isbn} {...book} />
          ))}
        </tbody>
      </table>
    </>
  );
};

export default BookList;
